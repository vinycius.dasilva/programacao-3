﻿namespace Atividade2;

public class Atividade2
{
    public static void Main(string[] args)
    {
        var floatValue = 4.0F;
        var intValue = 4;
        var stringValue = "My String";
        var booleanValue = true;

        // int para short
        var intValueToShort = ConvertValue<int, short>(intValue);
        PrintTypeAndValues(intValue, intValueToShort);

        // int para long
        var intValueToLong = ConvertValue<int, long>(intValue);
        PrintTypeAndValues(intValue, intValueToLong);

        // float para int
        var floatValueToInt = ConvertValue<float, int>(floatValue);
        PrintTypeAndValues(floatValue, floatValueToInt);

        // int para float
        var intValueToFloat = ConvertValue<int, float>(intValue);
        PrintTypeAndValues(intValue, intValueToFloat);

        // int para double
        var intValueToDouble = ConvertValue<int, double>(intValue);
        PrintTypeAndValues(intValue, intValueToDouble);

        // bool para string
        var boolValueToString = ConvertValue<bool, string>(booleanValue);
        PrintTypeAndValues(booleanValue, boolValueToString);

        // int para string
        var intValueToString = ConvertValue<int, string>(intValue);
        PrintTypeAndValues(intValue, intValueToString);

        // boxing (qualquer tipo para objeto)
        var stringValueToObject = ConvertValue<string, object>(stringValue);
        PrintTypeAndValues(stringValue, stringValueToObject);

        // unboxing (objeto para qualquer tipo)
        var objectValueToString = ConvertValue<object, string>(stringValueToObject);
        PrintTypeAndValues(stringValueToObject, objectValueToString);
    }

    /*
    Seguindo os princípios de SOLIC, cada metodo tem uma função específica de dada forma que só posso modificá-las
    sem danificar nada além deles e suas lógicas.
    */
    
    private static TNc ConvertValue<TOc, TNc>(TOc originConvertible)
    {
        var newConvertible = Convert.ChangeType(originConvertible, typeof(TNc));
        return (TNc)newConvertible!;
    }

    private static void PrintTypeAndValues<TOc, TNc>(TOc originConvertible, TNc newConvertible)
    {
        Console.WriteLine(
            $"Initial\tValue: {originConvertible}\n\tType: {typeof(TOc)}\nNew\tValue: {newConvertible}\n\tType: {typeof(TNc)}\n");
    }
}