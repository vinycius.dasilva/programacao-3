﻿namespace Atividade1;

public class Atividade1
{
    public static void Main(string[] args)
    {
        var firstName = args[0];
        var lastName = args[1];
        Console.WriteLine($"Hello {firstName} {lastName}!");
    }
}